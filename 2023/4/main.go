package main

import (
	"fmt"
	"os"
	"slices"
	"strconv"
	"strings"
)

type scratchcard struct {
	winningNumbers   []int
	scratchedNumbers []int
}

func main() {
	input, e := os.ReadFile(os.Args[1])
	if e != nil {
		panic(e)
	}

	totalPoints := GetTotalPoints(string(input))
	fmt.Printf("There are %d total points in the scratchcards.\n", totalPoints)

	totalNumberOfScratchCards := GetTotalNumberOfScratchCards(string(input))
	fmt.Printf("You win %d scratch cards in total.\n", totalNumberOfScratchCards)
}

func GetTotalNumberOfScratchCards(input string) int {
	inputLines := strings.Split(input, "\n")

	scratchcards := ParseInput(inputLines)

	numberOfScratchCards := map[int]int{}
	for i, scratchcard := range scratchcards {
		numberOfScratchCards[i]++

		numberOfMatches := GetNumberOfMatches(scratchcard)
		for k := i + 1; k <= i+numberOfMatches; k++ {
			numberOfScratchCards[k] += numberOfScratchCards[i]
		}
	}

	totalNumberOfScratchcards := 0
	for _, scratchCardCount := range numberOfScratchCards {
		totalNumberOfScratchcards += scratchCardCount
	}

	return totalNumberOfScratchcards
}

func GetNumberOfMatches(scratchcard scratchcard) int {
	matches := 0
	for _, scratchedNumber := range scratchcard.scratchedNumbers {
		if slices.Contains(scratchcard.winningNumbers, scratchedNumber) {
			matches++
		}
	}

	return matches
}

func GetTotalPoints(input string) int {
	inputLines := strings.Split(input, "\n")

	scratchcards := ParseInput(inputLines)

	totalPoints := 0
	for _, scratchcard := range scratchcards {
		totalPoints += GetPoints(scratchcard)
	}
	return totalPoints
}

func GetPoints(scratchcard scratchcard) int {
	points := 0
	for _, scratchedNumber := range scratchcard.scratchedNumbers {
		if slices.Contains(scratchcard.winningNumbers, scratchedNumber) {
			if points == 0 {
				points = 1
			} else {
				points *= 2
			}
		}
	}

	return points
}

func ParseInput(lines []string) []scratchcard {
	var scratchCards []scratchcard
	for _, line := range lines {
		winningNumbersStrings := strings.Split(strings.Split(strings.Split(line, ":")[1], "|")[0], " ")
		var winningNumbers []int
		for _, item := range winningNumbersStrings {
			if item != "" {
				number, e := strconv.Atoi(item)
				if e != nil {
					panic(e)
				}
				winningNumbers = append(winningNumbers, number)
			}
		}

		scratchedNumbersStrings := strings.Split(strings.Split(strings.Split(line, ":")[1], "|")[1], " ")
		var scratchedNumbers []int
		for _, item := range scratchedNumbersStrings {
			if item != "" {
				number, e := strconv.Atoi(item)
				if e != nil {
					panic(e)
				}
				scratchedNumbers = append(scratchedNumbers, number)
			}
		}
		scratchCards = append(scratchCards, scratchcard{winningNumbers: winningNumbers, scratchedNumbers: scratchedNumbers})
	}
	return scratchCards
}
