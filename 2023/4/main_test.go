package main

import (
	"fmt"
	"reflect"
	"testing"
)

func TestGetTotalNumberOfScratchCards(t *testing.T) {
	input := `Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
    Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
    Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
    Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
    Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
    Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11`

	const expectedTotalScratchcards = 30
	actualTotalScratchcards := GetTotalNumberOfScratchCards(input)

	if actualTotalScratchcards != expectedTotalScratchcards {
		t.Errorf("Expected %d total scratchcards but got %d total scratchcards.", expectedTotalScratchcards, actualTotalScratchcards)
	}
}

func TestGetNumberOfMatches(t *testing.T) {
	var tests = []struct {
		scratchcard scratchcard
		matches     int
	}{
		{scratchcard{
			[]int{41, 48, 83, 86, 17},
			[]int{83, 86, 6, 31, 17, 9, 48, 53},
		}, 4},
		{scratchcard{
			[]int{13, 32, 20, 16, 61},
			[]int{61, 30, 68, 82, 17, 32, 24, 19},
		}, 2},
		{scratchcard{
			[]int{41, 92, 73, 84, 69},
			[]int{59, 84, 76, 51, 58, 5, 54, 83},
		}, 1},
	}

	for _, test := range tests {
		testName := fmt.Sprint(test.scratchcard)
		t.Run(testName, func(t *testing.T) {
			actualMatches := GetNumberOfMatches(test.scratchcard)
			if actualMatches != test.matches {
				t.Errorf("Expected %d matches but got %d matches", test.matches, actualMatches)
			}
		})
	}
}

func TestGetTotalPoints(t *testing.T) {
	input := `Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
    Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
    Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
    Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
    Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
    Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11`

	const expectedTotalPoints = 13
	actualTotalPoints := GetTotalPoints(input)

	if actualTotalPoints != expectedTotalPoints {
		t.Errorf("Expected %d total points but got %d total points", expectedTotalPoints, actualTotalPoints)
	}
}

func TestGetPoints(t *testing.T) {
	var tests = []struct {
		scratchcard scratchcard
		points      int
	}{
		{scratchcard{
			[]int{41, 48, 83, 86, 17},
			[]int{83, 86, 6, 31, 17, 9, 48, 53},
		}, 8},
		{scratchcard{
			[]int{13, 32, 20, 16, 61},
			[]int{61, 30, 68, 82, 17, 32, 24, 19},
		}, 2},
		{scratchcard{
			[]int{41, 92, 73, 84, 69},
			[]int{59, 84, 76, 51, 58, 5, 54, 83},
		}, 1},
	}

	for _, test := range tests {
		testName := fmt.Sprint(test.scratchcard)
		t.Run(testName, func(t *testing.T) {
			actualPoints := GetPoints(test.scratchcard)
			if actualPoints != test.points {
				t.Errorf("Expected %d points but got %d points", test.points, actualPoints)
			}
		})
	}
}

func TestGetNumbersFromLine(t *testing.T) {
	var inputLines []string
	inputLines = append(inputLines, "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53")

	expectedScratchcards := []scratchcard{
		{[]int{41, 48, 83, 86, 17}, []int{83, 86, 6, 31, 17, 9, 48, 53}},
	}
	actualScratchcards := ParseInput(inputLines)

	if !reflect.DeepEqual(expectedScratchcards, actualScratchcards) {
		t.Error("Actual scratchcards did not equal expected scratchcards.")
	}
}
