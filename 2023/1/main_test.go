package main

import "testing"

func TestGetTotalValueWithNamedNumbers(t *testing.T) {
	input := `two1nine
	eightwothree
	abcone2threexyz
	xtwone3four
	4nineeightseven2
	zoneight234
	7pqrstsixteen`

	const expectedTotalValue = 281
	actualTotalValue := GetTotalValueWithNamedNumbers(input)

	if actualTotalValue != expectedTotalValue {
		t.Errorf("Expected %d but got %d", expectedTotalValue, actualTotalValue)
	}
}

func TestGetValueFromLineWithNamedNumbers(t *testing.T) {
	input := "abcone2threexyz"

	const expectedValue = 13
	actualValue := GetValueFromLineWithNamedNumbers(input)

	if actualValue != expectedValue {
		t.Errorf("Expected value to equal %d but was %d", expectedValue, actualValue)
	}
}

func TestGetTotalValue(t *testing.T) {
	input := `1abc2
	pqr3stu8vwx
	a1b2c3d4e5f
	treb7uchet`

	const expectedTotalValue = 142
	actualTotalValue := GetTotalValue(input)

	if actualTotalValue != expectedTotalValue {
		t.Errorf("Expected %d but got %d", expectedTotalValue, actualTotalValue)
	}
}

func TestGetValueFromLine(t *testing.T) {
	line := "a1b2c3d4e5f"

	const expectedValue = 15
	actualValue := GetValueFromLine(line)

	if actualValue != expectedValue {
		t.Errorf("Expected %d but got %d", expectedValue, actualValue)
	}
}
