package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"unicode"
)

type number struct {
	value int
	name  string
}

func main() {
	input, e := os.ReadFile(os.Args[1])
	if e != nil {
		panic(e)
	}

	totalValue := GetTotalValue(string(input))
	fmt.Printf("The total calibration value is: %d\n", totalValue)

	totalValueWithNamedNumbers := GetTotalValueWithNamedNumbers(string(input))
	fmt.Printf("The total calibration value (using named numbers) is: %d\n", totalValueWithNamedNumbers)
}

func GetTotalValueWithNamedNumbers(input string) int {
	lines := strings.Split(input, "\n")

	totalValue := 0
	for _, line := range lines {
		totalValue += GetValueFromLineWithNamedNumbers(line)
	}

	return totalValue
}

func GetValueFromLineWithNamedNumbers(line string) int {
	numbers := []number{}
	numbers = append(numbers, number{1, "one"})
	numbers = append(numbers, number{2, "two"})
	numbers = append(numbers, number{3, "three"})
	numbers = append(numbers, number{4, "four"})
	numbers = append(numbers, number{5, "five"})
	numbers = append(numbers, number{6, "six"})
	numbers = append(numbers, number{7, "seven"})
	numbers = append(numbers, number{8, "eight"})
	numbers = append(numbers, number{9, "nine"})

	valueString := ""
	for i, character := range line {
		if unicode.IsDigit(character) && len(valueString) == 0 {
			valueString = string(character)
		} else if unicode.IsDigit(character) {
			// Replace second digit of valueString
			valueString = valueString[0:1]
			valueString = valueString + string(character)
		} else {
			for k := i + 1; k <= len(line); k++ {
				for _, number := range numbers {
					if line[i:k] == number.name {
						if len(valueString) == 0 {
							valueString = fmt.Sprint(number.value)
						} else {
							// Replace second digit of valueString
							valueString = valueString[0:1]
							valueString = valueString + fmt.Sprint(number.value)
						}
					}
				}
			}
		}
	}

	// If there is only one digit in the line
	if len(valueString) == 1 {
		valueString = valueString + valueString
	}

	value, e := strconv.Atoi(valueString)
	if e != nil {
		panic(e)
	}

	return value
}

func GetTotalValue(input string) int {
	lines := strings.Split(input, "\n")

	totalValue := 0
	for _, line := range lines {
		totalValue += GetValueFromLine(line)
	}

	return totalValue
}

func GetValueFromLine(line string) int {
	valueString := ""
	for _, character := range line {
		if unicode.IsDigit(character) && len(valueString) == 0 {
			valueString = string(character)
		} else if unicode.IsDigit(character) {
			// Replace second digit of valueString
			valueString = valueString[0:1]
			valueString = valueString + string(character)
		}
	}

	// If there is only one digit in the line
	if len(valueString) == 1 {
		valueString = valueString + valueString
	}

	value, e := strconv.Atoi(valueString)
	if e != nil {
		panic(e)
	}

	return value
}
