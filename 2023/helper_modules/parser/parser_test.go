package parser

import (
	"reflect"
	"testing"
)

func TestGetNumbersFromLine(t *testing.T) {
	input := "   3   2   34   6543 213  "

	expectedNumbers := []int{3, 2, 34, 6543, 213}
	actualNumbers := GetNumbersFromLine(input)

	if !reflect.DeepEqual(actualNumbers, expectedNumbers) {
		t.Error("Actual array of numbers did not equal expected")
	}
}
