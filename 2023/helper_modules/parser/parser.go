package parser

import (
	"strconv"
	"strings"
)

func GetNumbersFromLine(input string) []int {
	var output []int
	strings := strings.Split(input, " ")
	for _, item := range strings {
		if item != "" {
			number, e := strconv.Atoi(item)
			if e != nil {
				panic(e)
			}
			output = append(output, number)
		}
	}
	return output
}
