package main

import (
	"fmt"
	parser "helperModules/parser"
	"os"
	"strconv"
	"strings"
)

type race struct {
	time           int
	recordDistance int
}

func main() {
	input, e := os.ReadFile(os.Args[1])
	if e != nil {
		panic(e)
	}

	marginOfError := GetMarginOfError(string(input))
	fmt.Printf("Margin of error: %d\n", marginOfError)

	winningPossibilties := GetPossibilitiesSingleRace(string(input))
	fmt.Printf("Winning Possibilities: %d\n", winningPossibilties)
}

func GetPossibilitiesSingleRace(input string) int {
	race := ParseInputSingleRace(input)

	return GetWinningPossibilitiesCount(race)
}

func ParseInputSingleRace(input string) race {
	lines := strings.Split(input, "\n")

	timeString := strings.ReplaceAll(strings.Split(lines[0], ":")[1], " ", "")
	time, e := strconv.Atoi(timeString)
	if e != nil {
		panic(e)
	}

	distanceString := strings.ReplaceAll(strings.Split(lines[1], ":")[1], " ", "")
	distance, e := strconv.Atoi(distanceString)
	if e != nil {
		panic(e)
	}

	return race{time, distance}
}

func GetMarginOfError(input string) int {
	races := ParseInput(input)

	marginOfError := 1
	for _, race := range races {
		marginOfError = marginOfError * GetWinningPossibilitiesCount(race)
	}

	return marginOfError
}

func GetWinningPossibilitiesCount(race race) int {
	winningPossibilitiesCount := 0
	for holdCount := 0; holdCount <= race.time; holdCount++ {
		distanceTravelled := holdCount * (race.time - holdCount)
		if distanceTravelled > race.recordDistance {
			winningPossibilitiesCount++
		}
	}

	return winningPossibilitiesCount
}

func ParseInput(input string) []race {
	lines := strings.Split(input, "\n")
	times := parser.GetNumbersFromLine(strings.Split(lines[0], ":")[1])
	distances := parser.GetNumbersFromLine(strings.Split(lines[1], ":")[1])

	var races []race
	for i, time := range times {
		races = append(races, race{time, distances[i]})
	}

	return races
}
