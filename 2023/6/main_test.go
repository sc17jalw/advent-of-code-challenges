package main

import (
	"fmt"
	"reflect"
	"testing"
)

func TestParseInputSingleRace(t *testing.T) {
	input := `Time:      7  15   30
Distance:  9  40  200`

	expectedRace := race{71530, 940200}

	actualRace := ParseInputSingleRace(input)

	if actualRace != expectedRace {
		t.Errorf("Expected: %d Actual: %d", expectedRace, actualRace)
	}
}

func TestGetMarginOfError(t *testing.T) {
	input := `Time:      7  15   30
Distance:  9  40  200`

	const expectedMarginOfError int = 288

	actualMarginOfError := GetMarginOfError(input)

	if actualMarginOfError != expectedMarginOfError {
		t.Errorf("Expected: %d Actual: %d", expectedMarginOfError, actualMarginOfError)
	}
}

func TestGetWinningPossibilitiesCount(t *testing.T) {
	var tests = []struct {
		race         race
		winningCount int
	}{
		{race{7, 9}, 4},
		{race{15, 40}, 8},
		{race{30, 200}, 9},
	}

	for _, test := range tests {
		testName := fmt.Sprintf("Input: %v. Expected output: %d", test.race, test.winningCount)
		t.Run(testName, func(t *testing.T) {
			actualWinningCount := GetWinningPossibilitiesCount(test.race)
			if actualWinningCount != test.winningCount {
				t.Errorf("Expected %d but got %d", test.winningCount, actualWinningCount)
			}
		})
	}
}

func TestParseInput(t *testing.T) {
	input := `Time:      7  15   30
Distance:  9  40  200`

	expectedRaces := []race{
		{7, 9},
		{15, 40},
		{30, 200},
	}

	actualRaces := ParseInput(input)

	if !reflect.DeepEqual(actualRaces, expectedRaces) {
		t.Error("Actual races did not equal expected")
	}
}
