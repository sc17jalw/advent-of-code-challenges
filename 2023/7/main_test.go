package main

import (
	"fmt"
	"os"
	"reflect"
	"testing"
)

func TestMain(t *testing.T) {
	input, e := os.ReadFile("./input.txt")
	if e != nil {
		panic(e)
	}

	totalWinningsWithJokers := GetTotalWinningsWithJokers(string(input))
	fmt.Printf("Total winnings with jokers: %d\n", totalWinningsWithJokers)
}

func TestGetTotalWinningsWithJokers(t *testing.T) {
	input := `32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483`

	const expectedOutput int = 5905

	actualOutput := GetTotalWinningsWithJokers(input)

	if actualOutput != 5905 {
		t.Errorf("Excpected: %d Actual: %d", expectedOutput, actualOutput)
	}
}

func TestSwapJokers(t *testing.T) {
	var tests = []struct {
		inputHand  string
		outputHand string
	}{
		{"T55J5", "T5555"},
		{"QQQJA", "QQQQA"},
		{"KTJJT", "KTTTT"},
		{"JJJJJ", "AAAAA"},
		{"TTTTT", "TTTTT"},
		{"23J65", "23665"},
		{"JJ234", "44234"},
		{"JJJJ4", "44444"},
		{"J2233", "32233"},
	}

	for _, test := range tests {
		testName := fmt.Sprintf("Input: %s. Expected output: %s", test.inputHand, test.outputHand)
		t.Run(testName, func(t *testing.T) {
			actualOutputHand := SwapJokers(test.inputHand)
			if actualOutputHand != test.outputHand {
				t.Errorf("Expected %s but got %s", test.outputHand, actualOutputHand)
			}
		})
	}
}

func TestGetTotalWinnings(t *testing.T) {
	input := `32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483`

	const expectedOutput int = 6440

	actualOutput := GetTotalWinnings(input)

	if actualOutput != expectedOutput {
		t.Errorf("Expected winnings: %d Actual winnings: %d", expectedOutput, actualOutput)
	}
}

func TestSortHandsByRankTwoPairs(t *testing.T) {
	inputHands := []hand{
		{"22JJ8", "22JJ8", 220},
		{"265JK", "265JK", 765},
	}

	expectedHands := []hand{
		{"265JK", "265JK", 765},
		{"22JJ8", "22JJ8", 220},
	}

	actualHands := SortHandsByRank(inputHands, HighestCardsWithoutJokers)

	if !reflect.DeepEqual(actualHands, expectedHands) {
		t.Errorf("Actual hands order: %v did not match expected: %v", actualHands, expectedHands)
	}
}

func TestSortHandsByRank(t *testing.T) {
	inputHands := []hand{
		{"T55J5", "T55J5", 684},
		{"KK677", "KK677", 28},
		{"KTJJT", "KTJJT", 220},
		{"QQQJA", "QQQJA", 483},
		{"32T3K", "32T3K", 765},
	}

	expectedHands := []hand{
		{"32T3K", "32T3K", 765},
		{"KTJJT", "KTJJT", 220},
		{"KK677", "KK677", 28},
		{"T55J5", "T55J5", 684},
		{"QQQJA", "QQQJA", 483},
	}

	actualHands := SortHandsByRank(inputHands, HighestCardsWithoutJokers)

	if !reflect.DeepEqual(actualHands, expectedHands) {
		t.Errorf("Actual hands order: %v did not match expected: %v", actualHands, expectedHands)
	}
}

func TestParseInputSingleRace(t *testing.T) {
	input := `32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483`

	expectedHands := []hand{
		{"32T3K", "32T3K", 765},
		{"T55J5", "T55J5", 684},
		{"KK677", "KK677", 28},
		{"KTJJT", "KTJJT", 220},
		{"QQQJA", "QQQJA", 483},
	}

	actualHands := ParseInput(input)

	if !reflect.DeepEqual(actualHands, expectedHands) {
		t.Errorf("Actual hands: %v did not match expected: %v", actualHands, expectedHands)
	}
}
