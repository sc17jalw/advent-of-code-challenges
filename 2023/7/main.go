package main

import (
	"fmt"
	"os"
	"slices"
	"strconv"
	"strings"
)

type hand struct {
	cards         string
	cardsOriginal string
	bid           int
}

type card struct {
	face rune
	rank int
}

func main() {
	input, e := os.ReadFile(os.Args[1])
	if e != nil {
		panic(e)
	}

	totalWinnings := GetTotalWinnings(string(input))
	fmt.Printf("Total winnings: %d\n", totalWinnings)

	totalWinningsWithJokers := GetTotalWinningsWithJokers(string(input))
	fmt.Printf("Total winnings with jokers: %d\n", totalWinningsWithJokers)
}

func GetTotalWinningsWithJokers(input string) int {
	hands := ParseInput(input)

	// Replace all jokers in the hands
	for i := range hands {
		hands[i].cards = SwapJokers(hands[i].cards)
	}

	hands = SortHandsByRank(hands, HighestCardsWithJokers)

	totalWinnings := 0
	for i, hand := range hands {
		totalWinnings += (i + 1) * hand.bid
	}

	return totalWinnings
}

func GetTotalWinnings(input string) int {
	hands := ParseInput(input)

	hands = SortHandsByRank(hands, HighestCardsWithoutJokers)

	totalWinnings := 0
	for i, hand := range hands {
		totalWinnings += (i + 1) * hand.bid
	}

	return totalWinnings
}

func HighestCardsWithJokers(a, b string) int {
	ranks := map[rune]int{
		'J': 1,
		'2': 2,
		'3': 3,
		'4': 4,
		'5': 5,
		'6': 6,
		'7': 7,
		'8': 8,
		'9': 9,
		'T': 10,
		'Q': 12,
		'K': 13,
		'A': 14,
	}

	for i, cardInA := range a {
		if ranks[cardInA] > ranks[rune(b[i])] {
			return 1
		} else if ranks[rune(b[i])] > ranks[cardInA] {
			return -1
		}
	}

	return 0
}

func SwapJokers(inputHand string) string {
	if CountOfRuneInString('J', inputHand) == 5 {
		return "AAAAA"
	}

	// Get count of cards in the hand
	cardCounts := make(map[rune]int)
	for _, card := range inputHand {
		if card != 'J' {
			cardCounts[card]++
		}
	}

	// Find most common card in the hand
	var mostCommonCard rune
	maxCount := 0
	mostCommonExists := false
	for card, count := range cardCounts {
		if count > maxCount {
			maxCount = count
			mostCommonCard = card
			mostCommonExists = true
		} else if count == maxCount {
			mostCommonExists = false
		}
	}

	// If we need to select the best card as there is no most common card
	if mostCommonExists {
		return strings.ReplaceAll(inputHand, "J", string(mostCommonCard))
	} else {
		var highestCard rune
		for card := range cardCounts {
			if highestCard == 0 {
				highestCard = card
			} else {
				if HighestCardsWithoutJokers(string(highestCard), string(card)) == -1 {
					highestCard = card
				}
			}
		}
		return strings.ReplaceAll(inputHand, "J", string(highestCard))
	}
}

func SortHandsByRank(inputHands []hand, HighestCards func(string, string) int) []hand {
	slices.SortFunc(inputHands, func(a, b hand) int {
		aFiveOfKind := false
		bFiveOfKind := false

		aFourOfAKind := false
		bFourOfAKind := false

		aThreeOfAKind := false
		bThreeOfAKind := false

		aPair := false
		bPair := false

		aTwoPair := false
		var aPairCard rune
		bTwoPair := false
		var bPairCard rune

		// Count pairs a
		for _, character := range a.cards {
			if CountOfRuneInString(character, a.cards) == 5 {
				aFiveOfKind = true
			}

			if CountOfRuneInString(character, a.cards) == 4 {
				aFourOfAKind = true
			}

			if CountOfRuneInString(character, a.cards) == 3 {
				aThreeOfAKind = true
			}

			if CountOfRuneInString(character, a.cards) == 2 {
				if aPairCard == 0 {
					aPair = true
					aPairCard = character
				} else if aPairCard != character {
					aTwoPair = true
				}
			}
		}

		// Count pairs b
		for _, character := range b.cards {
			if CountOfRuneInString(character, b.cards) == 5 {
				bFiveOfKind = true
			}

			if CountOfRuneInString(character, b.cards) == 4 {
				bFourOfAKind = true
			}

			if CountOfRuneInString(character, b.cards) == 3 {
				bThreeOfAKind = true
			}

			if CountOfRuneInString(character, b.cards) == 2 {
				if bPairCard == 0 {
					bPair = true
					bPairCard = character
				} else if bPairCard != character {
					bTwoPair = true
				}
			}
		}

		// Is five of a kind?
		if aFiveOfKind && !bFiveOfKind {
			return 1
		} else if bFiveOfKind && !aFiveOfKind {
			return -1
		} else if aFiveOfKind && bFiveOfKind {
			return HighestCards(a.cardsOriginal, b.cardsOriginal)
		}

		// Is four of a kind?
		if aFourOfAKind && !bFourOfAKind {
			return 1
		} else if bFourOfAKind && !aFourOfAKind {
			return -1
		} else if aFourOfAKind && bFourOfAKind {
			return HighestCards(a.cardsOriginal, b.cardsOriginal)
		}

		// Is full house?
		if (aThreeOfAKind && aPair) && !(bThreeOfAKind && bPair) {
			return 1
		} else if (bThreeOfAKind && bPair) && !(aThreeOfAKind && aPair) {
			return -1
		} else if (aThreeOfAKind && aPair) && (bThreeOfAKind && bPair) {
			return HighestCards(a.cardsOriginal, b.cardsOriginal)
		}

		// Is three of a kind?
		if aThreeOfAKind && !bThreeOfAKind {
			return 1
		} else if bThreeOfAKind && !aThreeOfAKind {
			return -1
		} else if aThreeOfAKind && bThreeOfAKind {
			return HighestCards(a.cardsOriginal, b.cardsOriginal)
		}

		// Is two pair?
		if aTwoPair && !bTwoPair {
			return 1
		} else if bTwoPair && !aTwoPair {
			return -1
		} else if aTwoPair && bTwoPair {
			return HighestCards(a.cardsOriginal, b.cardsOriginal)
		}

		// Is one pair?
		if aPair && !bPair {
			return 1
		} else if bPair && !aPair {
			return -1
		} else if aPair && bPair {
			return HighestCards(a.cardsOriginal, b.cardsOriginal)
		}

		// Highest card
		return HighestCards(a.cardsOriginal, b.cardsOriginal)
	})
	return inputHands
}

func HighestCardsWithoutJokers(a, b string) int {
	ranks := map[rune]int{
		'2': 2,
		'3': 3,
		'4': 4,
		'5': 5,
		'6': 6,
		'7': 7,
		'8': 8,
		'9': 9,
		'T': 10,
		'J': 11,
		'Q': 12,
		'K': 13,
		'A': 14,
	}

	for i, cardInA := range a {
		if ranks[cardInA] > ranks[rune(b[i])] {
			return 1
		} else if ranks[rune(b[i])] > ranks[cardInA] {
			return -1
		}
	}

	return 0
}

func CountOfRuneInString(r rune, s string) int {
	count := 0
	for _, character := range s {
		if character == r {
			count++
		}
	}
	return count
}

func ParseInput(input string) []hand {
	lines := strings.Split(input, "\n")

	var hands []hand
	for _, line := range lines {
		cards := strings.Split(line, " ")[0]
		bid, e := strconv.Atoi(strings.Split(line, " ")[1])
		if e != nil {
			panic(e)
		}

		hands = append(hands, hand{cards, cards, bid})
	}

	return hands
}
