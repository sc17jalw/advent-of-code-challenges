package main

import "testing"

func TestGetSumOfPrecedingNumbers(t *testing.T) {
	input := `0 3 6 9 12 15
1 3 6 10 15 21`

	expectedOutput := -3

	actualOutput := GetSumOfPrecedingNumbers(input)

	if actualOutput != expectedOutput {
		t.Errorf("Expected: %d, Actual %d", expectedOutput, actualOutput)
	}
}

func TestGetPrecedingNumberFromLine(t *testing.T) {
	inputLine := "0 3 6 9 12 15"

	actualPrecedingNumber := GetPrecedingNumberFromLine(inputLine)

	expectedPrecedingNumber := -3

	if actualPrecedingNumber != expectedPrecedingNumber {
		t.Errorf("Actual: %d, Expected: %d", actualPrecedingNumber, expectedPrecedingNumber)
	}
}

func TestGetSumOfNextNumbers(t *testing.T) {
	input := `0 3 6 9 12 15
1 3 6 10 15 21`

	expectedOutput := 46

	actualOutput := GetSumOfNextNumbers(input)

	if actualOutput != expectedOutput {
		t.Errorf("Expected: %d, Actual %d", expectedOutput, actualOutput)
	}
}
func TestGetNextNumberFromLine(t *testing.T) {
	inputLine := "0 3 6 9 12 15"

	actualNextNumber := GetNextNumberFromLine(inputLine)

	expectedNextNumber := 18

	if actualNextNumber != expectedNextNumber {
		t.Errorf("Actual: %d, Expected: %d", actualNextNumber, expectedNextNumber)
	}
}
