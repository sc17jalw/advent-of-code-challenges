package main

import (
	"fmt"
	parser "helperModules/parser"
	"os"
	"strings"
)

func main() {
	input, e := os.ReadFile(os.Args[1])
	if e != nil {
		panic(e)
	}

	sumOfNextNumbers := GetSumOfNextNumbers(string(input))
	fmt.Printf("Sum of next numbers: %d\n", sumOfNextNumbers)

	sumOfPrecedingNumbers := GetSumOfPrecedingNumbers(string(input))
	fmt.Printf("Sum of preceding numbers: %d\n", sumOfPrecedingNumbers)

}

func GetSumOfPrecedingNumbers(input string) int {
	lines := strings.Split(input, "\n")

	sum := 0
	for _, line := range lines {
		sum += GetPrecedingNumberFromLine(line)
	}

	return sum
}

func GetPrecedingNumberFromLine(line string) int {
	numbers := parser.GetNumbersFromLine(line)

	var differences [][]int
	differences = append(differences, numbers)
	for {
		differenceList := []int{}
		for i := 0; i+1 < len(numbers); i++ {
			differenceList = append(differenceList, numbers[i+1]-numbers[i])
		}
		differences = append(differences, differenceList)

		// Check if all zeros
		allZeros := true
		for _, item := range differenceList {
			if item != 0 {
				allZeros = false
				break
			}
		}

		if allZeros {
			break
		} else {
			numbers = differenceList
		}
	}

	for i := len(differences) - 1; i > 0; i-- {
		differences[i-1] = append([]int{differences[i-1][0] - differences[i][0]}, differences[i-1]...)
	}
	return differences[0][0]
}

func GetSumOfNextNumbers(input string) int {
	lines := strings.Split(input, "\n")

	sum := 0
	for _, line := range lines {
		sum += GetNextNumberFromLine(line)
	}

	return sum
}

func GetNextNumberFromLine(line string) int {
	numbers := parser.GetNumbersFromLine(line)

	var differences [][]int
	differences = append(differences, numbers)
	for {
		differenceList := []int{}
		for i := 0; i+1 < len(numbers); i++ {
			differenceList = append(differenceList, numbers[i+1]-numbers[i])
		}
		differences = append(differences, differenceList)

		// Check if all zeros
		allZeros := true
		for _, item := range differenceList {
			if item != 0 {
				allZeros = false
				break
			}
		}

		if allZeros {
			break
		} else {
			numbers = differenceList
		}
	}

	for i := len(differences) - 1; i > 0; i-- {
		differences[i-1] = append(differences[i-1], differences[i-1][len(differences[i-1])-1]+differences[i][len(differences[i])-1])
	}
	return differences[0][len(differences[0])-1]
}
