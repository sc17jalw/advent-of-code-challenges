package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"unicode"
)

type number struct {
	value      int
	startIndex int
	endIndex   int
}

func main() {
	input, e := os.ReadFile(os.Args[1])
	if e != nil {
		panic(e)
	}

	lines := strings.Split(string(input), "\n")

	totalPartNumbers := GetTotalPartNumbers(lines)
	fmt.Printf("The total number is: %d\n", totalPartNumbers)

	totGearRatios := GetTotalGearRatios(lines)
	fmt.Printf("The total gear ratio is: %d\n", totGearRatios)
}

func GetGearRatio(gearIndex int, adjacentLines []string) int {
	var endLineIndex int = 2
	if len(adjacentLines) == 2 {
		endLineIndex = 1
	}

	var startIndex, endIndex int = gearIndex, gearIndex
	// If not at the start of the line, decrease the start index
	if gearIndex != 0 {
		startIndex--
	}

	// If not at the end of the line, increase the end index
	if endIndex != len(adjacentLines[0])-1 {
		endIndex++
	}

	var adjacentNumbers []int
	for checkLineIndex := 0; checkLineIndex <= endLineIndex; checkLineIndex++ {
		numbersInLine := GetNumbersFromLine(adjacentLines[checkLineIndex])
		for _, number := range numbersInLine {
			if (number.startIndex <= gearIndex+1) && (number.endIndex >= gearIndex-1) {
				adjacentNumbers = append(adjacentNumbers, number.value)
			}
		}
	}

	// If gear is only adjacent to one number then it has no ratio.
	if len(adjacentNumbers) <= 1 {
		return 0
	}

	// Get product of adjacent numbers
	product := -1
	for _, number := range adjacentNumbers {
		if product == -1 {
			product = number
		} else {
			product = product * number
		}
	}
	return product
}

func GetTotalGearRatios(lines []string) int {
	totalGearRatio := 0
	for lineNumber, line := range lines {
		lineGears := GetGearIndicesInLine(line)

		for _, gear := range lineGears {
			if lineNumber == 0 {
				totalGearRatio += GetGearRatio(gear, lines[lineNumber:lineNumber+2])
			} else if lineNumber == len(lines)-1 {
				totalGearRatio += GetGearRatio(gear, lines[lineNumber-1:lineNumber+1])
			} else {
				totalGearRatio += GetGearRatio(gear, lines[lineNumber-1:lineNumber+2])
			}
		}
	}
	return totalGearRatio
}

func GetGearIndicesInLine(line string) []int {
	var gearIndeces []int
	for i, character := range line {
		if character == '*' {
			gearIndeces = append(gearIndeces, i)
		}
	}
	return gearIndeces
}

func GetTotalPartNumbers(lines []string) int {
	totalPartNumbers := 0
	for lineNumber, line := range lines {
		lineNumbers := GetNumbersFromLine(line)

		for _, number := range lineNumbers {
			if lineNumber == 0 {
				if IsPartNumber(number.startIndex, number.endIndex, lines[lineNumber:lineNumber+2]) {
					totalPartNumbers += number.value
				}
			} else if lineNumber == len(lines)-1 {
				if IsPartNumber(number.startIndex, number.endIndex, lines[lineNumber-1:lineNumber+1]) {
					totalPartNumbers += number.value
				}
			} else {
				if IsPartNumber(number.startIndex, number.endIndex, lines[lineNumber-1:lineNumber+2]) {
					totalPartNumbers += number.value
				}
			}
		}
	}
	return totalPartNumbers
}

func GetNumbersFromLine(line string) []number {
	numbers := []number{}
	var startIndex, endIndex int
	var readingNumber bool = false
	for i, character := range line {
		if unicode.IsDigit(character) {
			if !readingNumber {
				readingNumber = true
				startIndex = i
			}
			endIndex = i - 1
		} else {
			if readingNumber {
				readingNumber = false
				endIndex = i - 1

				numberValue, _ := strconv.Atoi(line[startIndex : endIndex+1])
				numbers = append(numbers, number{numberValue, startIndex, endIndex})
			}
		}
	}

	if readingNumber {
		endIndex++
		numberValue, _ := strconv.Atoi(line[startIndex : endIndex+1])
		numbers = append(numbers, number{numberValue, startIndex, endIndex})
	}

	return numbers
}

func IsPartNumber(startIndex int, endIndex int, adjacentLines []string) bool {
	var isPartNumber bool = false

	var endLineIndex int = 2
	if len(adjacentLines) == 2 {
		endLineIndex = 1
	}

	// If not at the start of the line, decrease the start index
	if startIndex != 0 {
		startIndex--
	}

	// If not at the end of the line, increase the end index
	if endIndex != len(adjacentLines[0])-1 {
		endIndex++
	}

	for checkLineIndex := 0; checkLineIndex <= endLineIndex; checkLineIndex++ {
		for i := startIndex; i <= endIndex; i++ {
			if (adjacentLines[checkLineIndex][i] != '.') && !unicode.IsDigit(rune(adjacentLines[checkLineIndex][i])) {
				isPartNumber = true
				break
			}
			if isPartNumber {
				break
			}
		}
	}

	return isPartNumber
}
