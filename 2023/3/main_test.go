package main

import (
	"reflect"
	"testing"
)

func TestGetNumbersFromLine(t *testing.T) {
	line := "1..2...33......6"
	expectedNumbers := []number{
		{1, 0, 0},
		{2, 3, 3},
		{33, 7, 8},
		{6, 15, 15},
	}
	actualNumbers := GetNumbersFromLine(line)

	if !reflect.DeepEqual(expectedNumbers, actualNumbers) {
		t.Error("Actual number did not equal expected numbers.")
	}
}

func TestIsPartNumberMiddleLine(t *testing.T) {
	var inputLines []string
	inputLines = append(inputLines, "..5.....$.......*.....")
	inputLines = append(inputLines, "123&..45...23..76.3.(7")
	inputLines = append(inputLines, ".....9.........*......")

	const expectedSum = 256
	actualSum := GetTotalPartNumbers(inputLines)

	if expectedSum != actualSum {
		t.Errorf("Expected sum to equal: %d but was: %d", expectedSum, actualSum)
	}
}

func TestIsPartNumberStartLine(t *testing.T) {
	var inputLines []string
	inputLines = append(inputLines, "123&..45...23..76.3.(7")
	inputLines = append(inputLines, "...7.......*...8......")

	const expectedSum = 160
	actualSum := GetTotalPartNumbers(inputLines)

	if expectedSum != actualSum {
		t.Errorf("Expected sum to equal: %d but was: %d", expectedSum, actualSum)
	}
}

func TestIsPartNumberEndLine(t *testing.T) {
	var inputLines []string
	inputLines = append(inputLines, "........$...7.........")
	inputLines = append(inputLines, "123&..45...23..76.3.(7")

	const expectedSum = 175
	actualSum := GetTotalPartNumbers(inputLines)

	if expectedSum != actualSum {
		t.Errorf("Expected sum to equal: %d but was: %d", expectedSum, actualSum)
	}
}

func TestIsPartNumberSingleNumber(t *testing.T) {
	var inputLines []string
	inputLines = append(inputLines, "........")
	inputLines = append(inputLines, "...23...")
	inputLines = append(inputLines, ".....*..")

	const expectedPartNumber = true
	actualPartNumber := IsPartNumber(3, 4, inputLines)

	if expectedPartNumber != actualPartNumber {
		t.Errorf("Expected sum to equal: %t but was: %t", expectedPartNumber, actualPartNumber)
	}
}

func TestGearRatio(t *testing.T) {
	var inputLines []string
	inputLines = append(inputLines, "...8...")
	inputLines = append(inputLines, "..4*...")
	inputLines = append(inputLines, "....22.")

	const expectedGearRatio = 704
	actualGearRatio := GetGearRatio(3, inputLines)

	if expectedGearRatio != actualGearRatio {
		t.Errorf("Expected gear ratio to equal: %d but was: %d", expectedGearRatio, actualGearRatio)
	}
}

func TestGetTotalGearRatios(t *testing.T) {
	var inputLines []string
	inputLines = append(inputLines, "467..114..")
	inputLines = append(inputLines, "...*......")
	inputLines = append(inputLines, "..35..633.")
	inputLines = append(inputLines, "......#...")
	inputLines = append(inputLines, "617*......")
	inputLines = append(inputLines, ".....+.58.")
	inputLines = append(inputLines, "..592.....")
	inputLines = append(inputLines, "......755.")
	inputLines = append(inputLines, "...$.*....")
	inputLines = append(inputLines, ".664.598..")

	const expectedTotal = 467835
	actualTotal := GetTotalGearRatios(inputLines)

	if expectedTotal != actualTotal {
		t.Errorf("Expected total to be: %d but was: %d", expectedTotal, actualTotal)
	}
}
