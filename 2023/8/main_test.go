package main

import (
	"reflect"
	"testing"
)

func TestGetNumberOfStepsGhosts(t *testing.T) {
	input := `LR

11A = (11B, XXX)
11B = (XXX, 11Z)
11Z = (11B, XXX)
22A = (22B, XXX)
22B = (22C, 22C)
22C = (22Z, 22Z)
22Z = (22B, 22B)
XXX = (XXX, XXX)`

	actualSteps := GetNumberOfStepsGhost(input, "AAA", "ZZZ")
	expectedSteps := 6

	if actualSteps != expectedSteps {
		t.Errorf("Actual steps: %d did not equal expected: %d", actualSteps, expectedSteps)
	}

}

func TestGetNumberOfSteps(t *testing.T) {
	input := `LLR

AAA = (BBB, BBB)
BBB = (AAA, ZZZ)
ZZZ = (ZZZ, ZZZ)`

	actualSteps := GetNumberOfSteps(input, "AAA", "ZZZ")
	expectedSteps := 6

	if actualSteps != expectedSteps {
		t.Errorf("Actual steps: %d did not equal expected: %d", actualSteps, expectedSteps)
	}

}

func TestParseInput(t *testing.T) {
	input := `RL

AAA = (BBB, CCC)
BBB = (DDD, EEE)
CCC = (ZZZ, GGG)
DDD = (DDD, DDD)
EEE = (EEE, EEE)
GGG = (GGG, GGG)
ZZZ = (ZZZ, ZZZ)`

	expectedSequence := "RL"

	expectedInstructions := make(map[string]leftRight)
	expectedInstructions["AAA"] = leftRight{"BBB", "CCC"}
	expectedInstructions["BBB"] = leftRight{"DDD", "EEE"}
	expectedInstructions["CCC"] = leftRight{"ZZZ", "GGG"}
	expectedInstructions["DDD"] = leftRight{"DDD", "DDD"}
	expectedInstructions["EEE"] = leftRight{"EEE", "EEE"}
	expectedInstructions["GGG"] = leftRight{"GGG", "GGG"}
	expectedInstructions["ZZZ"] = leftRight{"ZZZ", "ZZZ"}

	actualSequence, actualInstructions := ParseInput(input)

	if actualSequence != expectedSequence {
		t.Errorf("Actual sequence %s did not equal expected: %s", actualSequence, expectedSequence)
	}

	if !reflect.DeepEqual(actualInstructions, expectedInstructions) {
		t.Errorf("Actual instructions: %v did not equal expected: %v", actualInstructions, expectedInstructions)
	}
}
