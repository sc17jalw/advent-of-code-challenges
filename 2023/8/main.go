package main

import (
	"fmt"
	"os"
	"strings"
)

type leftRight struct {
	left  string
	right string
}

func main() {
	input, e := os.ReadFile(os.Args[1])
	if e != nil {
		panic(e)
	}

	var startNode string
	fmt.Print("Please enter the start node: ")
	fmt.Scan(&startNode)

	var endNode string
	fmt.Print("Please enter the end node: ")
	fmt.Scan(&endNode)

	steps := GetNumberOfSteps(string(input), startNode, endNode)
	fmt.Printf("The number of steps is: %d\n", steps)

	stepsGhosts := GetNumberOfStepsGhost(string(input), startNode, endNode)
	fmt.Printf("The number of steps (ghosts) is: %d\n", stepsGhosts)
}

func GetNumberOfStepsGhost(input string, startingNode string, destinationNode string) int {
	sequence, instructions := ParseInput(input)
	startingNodes := GetNodesEndingIn(instructions, rune(startingNode[2]))

	var stepsList []int
	for _, node := range startingNodes {
		steps := 0
		currentNode := node
		for {
			if currentNode[2] == destinationNode[2] {
				break
			} else {
				step := sequence[steps%len(sequence)]
				steps++
				if step == 'L' {
					currentNode = instructions[currentNode].left
				} else if step == 'R' {
					currentNode = instructions[currentNode].right
				}
			}
		}
		stepsList = append(stepsList, steps)
	}

	// Calculate Lowest Common Multiple
	result := stepsList[0]
	for _, step := range stepsList {
		result = lcm(result, step)
	}

	return result
}

func lcm(num1 int, num2 int) int {
	return num1 / gcd(num1, num2) * num2
}

func gcd(num1 int, num2 int) int {
	var gcd int
	for i := 1; i <= num1 && i <= num2; i++ {
		if num1%i == 0 && num2%i == 0 {
			gcd = i
		}
	}

	return gcd
}

func GetNodesEndingIn(instructions map[string]leftRight, character rune) []string {
	var nodes []string
	for node := range instructions {
		if rune(node[2]) == character {
			nodes = append(nodes, node)
		}
	}
	return nodes
}

func GetNumberOfSteps(input string, startingNode string, destinationNode string) int {
	sequence, instructions := ParseInput(input)

	steps := 0
	currentNode := startingNode
	for {
		if currentNode == destinationNode {
			break
		} else {
			step := sequence[steps%len(sequence)]
			steps++
			if step == 'L' {
				currentNode = instructions[currentNode].left
			} else if step == 'R' {
				currentNode = instructions[currentNode].right
			}
		}
	}

	return steps
}

func ParseInput(input string) (string, map[string]leftRight) {
	lines := strings.Split(input, "\n")
	sequence := lines[0]

	instructions := make(map[string]leftRight)
	for _, line := range lines[2:] {
		nodeName := strings.Split(line, "=")[0]
		nodeName = strings.TrimSpace(nodeName)

		destinationString := strings.TrimSpace(strings.Split(line, "=")[1])
		destinationString = strings.TrimFunc(destinationString, func(r rune) bool { return r == '(' || r == ')' })

		destintions := strings.Split(destinationString, ", ")
		instructions[nodeName] = leftRight{destintions[0], destintions[1]}
	}
	return sequence, instructions
}
