package main

import (
	"fmt"
	"reflect"
	"testing"
)

func TestGetLowestLocationFromRangeMultiThreaded(t *testing.T) {
	const input = `seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4`

	actualLowestLocation := GetLowestLocationFromRangeMultiThreaded(input)

	expectedLowestLocation := 46
	if actualLowestLocation != expectedLowestLocation {
		t.Errorf("Expected lowest location %d but was %d", expectedLowestLocation, actualLowestLocation)
	}
}

func TestGetLowestLocationFromRange(t *testing.T) {
	const input = `seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4`

	actualLowestLocation := GetLowestLocationFromRange(input)

	expectedLowestLocation := 46
	if actualLowestLocation != expectedLowestLocation {
		t.Errorf("Expected lowest location %d but was %d", expectedLowestLocation, actualLowestLocation)
	}
}

func TestGetLowestLocationMultiThreaded(t *testing.T) {
	const input = `seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4`

	actualLowestLocation := GetLowestLocationMultiThreaded(input)

	expectedLowestLocation := 35
	if actualLowestLocation != expectedLowestLocation {
		t.Errorf("Expected lowest location %d but was %d", expectedLowestLocation, actualLowestLocation)
	}
}

func TestGetLowestLocation(t *testing.T) {
	const input = `seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4`

	actualLowestLocation := GetLowestLocation(input)

	expectedLowestLocation := 35
	if actualLowestLocation != expectedLowestLocation {
		t.Errorf("Expected lowest location %d but was %d", expectedLowestLocation, actualLowestLocation)
	}
}

func TestParseInput(t *testing.T) {
	const input = `seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4`

	actualSeeds, actualMaps := ParseInput(input)

	// Check seeds returned
	expectedSeeds := []int{79, 14, 55, 13}
	if !reflect.DeepEqual(actualSeeds, expectedSeeds) {
		t.Error("Actual seeds did not equal the expected seeds")
	}

	// Check maps returned
	expectedMaps := [][]conversion{}
	expectedMaps = append(expectedMaps, []conversion{
		{50, 98, 2},
		{52, 50, 48},
	})

	expectedMaps = append(expectedMaps, []conversion{
		{0, 15, 37},
		{37, 52, 2},
		{39, 0, 15},
	})

	expectedMaps = append(expectedMaps, []conversion{
		{49, 53, 8},
		{0, 11, 42},
		{42, 0, 7},
		{57, 7, 4},
	})

	if !reflect.DeepEqual(actualMaps, expectedMaps) {
		t.Error("Actual maps did not equal the expected maps")
	}
}

func TestGetCorrespondingValue(t *testing.T) {
	conversions := []conversion{
		{50, 98, 2},
		{52, 50, 48},
	}

	var tests = []struct {
		inputValue     int
		convertedValue int
	}{
		{79, 81},
		{14, 14},
		{55, 57},
		{98, 50},
	}

	for _, test := range tests {
		testName := fmt.Sprintf("Input: %d. Expected output: %d", test.inputValue, test.convertedValue)
		t.Run(testName, func(t *testing.T) {
			actualConvertedValue := GetCorrespondingValue(test.inputValue, conversions)
			if actualConvertedValue != test.convertedValue {
				t.Errorf("Expected %d but got %d", test.convertedValue, actualConvertedValue)
			}
		})
	}
}
