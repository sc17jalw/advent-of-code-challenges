package main

import (
	"fmt"
	parser "helperModules/parser"
	"os"
	"reflect"
	"strings"
	"sync"
	"time"
)

type conversion struct {
	destinationRangeStart int
	sourceRangeStart      int
	rangeValue            int
}

type Location struct {
	mu    sync.Mutex
	value int
}

func main() {
	input, e := os.ReadFile(os.Args[1])
	if e != nil {
		panic(e)
	}

	timeStart := time.Now()
	lowestLocation := GetLowestLocation(string(input))
	timeTakenSingle := time.Now().Sub(timeStart)
	fmt.Printf("The lowest location is %d\nTime Taken: %d nanoseconds\n\n", lowestLocation, timeTakenSingle.Nanoseconds())

	timeStart = time.Now()
	lowestLocationMulti := GetLowestLocationMultiThreaded(string(input))
	timeTakenMulti := time.Now().Sub(timeStart)
	fmt.Printf(
		"When multithreading, the lowest location is %d\nTime Taken: %d nanoseconds\nSpeedup: %d nanoseconds\nSpeedup factor: %fx\n\n",
		lowestLocationMulti,
		timeTakenMulti.Nanoseconds(),
		time.Duration(timeTakenSingle.Nanoseconds())-time.Duration(timeTakenMulti.Nanoseconds()),
		float64(time.Duration(timeTakenSingle.Nanoseconds()))/float64(time.Duration(timeTakenMulti.Nanoseconds())),
	)

	fmt.Printf("------------------\nFinding the lowest location for the ranges:\n%s\n", strings.Split(string(input), "\n")[0])
	fmt.Print("This might take a while...\n\n")
	timeStart = time.Now()
	lowestLocationFromRange := GetLowestLocationFromRange(string(input))
	timeTakenRangeSingle := time.Now().Sub(timeStart)
	fmt.Printf("The lowest location using a range is %d\nTime taken: %d milliseconds\n\n", lowestLocationFromRange, timeTakenRangeSingle.Milliseconds())

	fmt.Printf("------------------\nFinding the lowest location for the ranges (multithreaded):\n%s\n", strings.Split(string(input), "\n\n")[0])
	timeStart = time.Now()
	lowestLocationFromRangeMulti := GetLowestLocationFromRangeMultiThreaded(string(input))
	timeTakenRangeMulti := time.Now().Sub(timeStart)
	fmt.Printf(
		"When multithreading, the lowest location using a range is %d\nTime Taken: %d milliseconds\nSpeedup: %d milliseconds\nSpeedup factor: %fx\n\n",
		lowestLocationFromRangeMulti,
		timeTakenRangeMulti.Milliseconds(),
		time.Duration(timeTakenRangeSingle.Milliseconds())-time.Duration(timeTakenRangeMulti.Milliseconds()),
		float64(time.Duration(timeTakenRangeSingle.Nanoseconds()))/float64(time.Duration(timeTakenRangeMulti.Nanoseconds())),
	)
}

func GetLowestLocationFromRangeMultiThreaded(input string) int {
	seeds, maps := ParseInput(input)
	lowestLocationValue := Location{
		value: GetLocation(seeds[0], maps),
	}

	EvaluateLocations := func(seeds []int, i int, maps [][]conversion, currentLowestLocation *Location, wg *sync.WaitGroup) {
		lowestLocation := GetLocation(seeds[i], maps)
		for seed := seeds[i]; seed < (seeds[i] + seeds[i+1]); seed++ {
			location := GetLocation(seed, maps)

			if location < lowestLocation {
				lowestLocation = location
			}
		}

		// Check if there is a queue for the mutex, and while there is keep checking the currentLowestValue
		for !currentLowestLocation.mu.TryLock() {
			if lowestLocation >= currentLowestLocation.value {
				wg.Done()
				return
			}
		}

		// Check if the endLocation found is the lowest yet by checking the shared variable
		if lowestLocation < currentLowestLocation.value {
			currentLowestLocation.value = lowestLocation
		}
		currentLowestLocation.mu.Unlock()
		wg.Done()
	}

	var wg sync.WaitGroup
	for i := 0; i < len(seeds); i += 2 {
		wg.Add(1)
		go EvaluateLocations(seeds, i, maps, &lowestLocationValue, &wg)
	}
	wg.Wait()

	if !lowestLocationValue.mu.TryLock() {
		panic("Mutex lowestLocation value still in use after all goroutines have finished.")
	}
	lowestLocationValue.mu.Unlock()

	return lowestLocationValue.value
}

func GetLowestLocationFromRange(input string) int {
	seeds, maps := ParseInput(input)
	lowestLocationValue := -1
	for i := 0; i < len(seeds); i += 2 {
		for seed := seeds[i]; seed < (seeds[i] + seeds[i+1]); seed++ {
			location := GetLocation(seed, maps)
			if (location < lowestLocationValue) || (lowestLocationValue == -1) {
				lowestLocationValue = location
			}
		}
	}

	return lowestLocationValue
}

func GetLowestLocationMultiThreaded(input string) int {
	seeds, maps := ParseInput(input)
	lowestLocationValue := Location{
		value: GetLocation(seeds[0], maps),
	}

	var wg sync.WaitGroup
	AddLocation := func(seed int, maps [][]conversion, lowestLocationContainer *Location, wg *sync.WaitGroup) {
		endLocation := GetLocation(seed, maps)

		lowestLocationContainer.mu.Lock()
		if endLocation < lowestLocationContainer.value {
			lowestLocationContainer.value = endLocation
		}
		lowestLocationContainer.mu.Unlock()
		wg.Done()
	}

	for _, seed := range seeds {
		wg.Add(1)
		go AddLocation(seed, maps, &lowestLocationValue, &wg)
	}
	wg.Wait()

	return lowestLocationValue.value
}

func GetLowestLocation(input string) int {
	seeds, maps := ParseInput(input)
	lowestLocationValue := -1
	for _, seed := range seeds {
		location := GetLocation(seed, maps)
		if (location < lowestLocationValue) || (lowestLocationValue == -1) {
			lowestLocationValue = location
		}
	}

	return lowestLocationValue
}

func GetLocation(seed int, maps [][]conversion) int {
	var location int = seed
	for _, conversions := range maps {
		location = GetCorrespondingValue(location, conversions)
	}

	return location
}

func ParseInput(input string) ([]int, [][]conversion) {
	lines := strings.Split(input, "\n")

	seedsString := strings.Split(lines[0], ":")[1]
	seeds := parser.GetNumbersFromLine(seedsString)

	maps := [][]conversion{}
	var currentMap []conversion
	for _, line := range lines[2:] {
		if line != "" && (!strings.Contains(line, "map:")) {
			// Reading line with numbers
			numbers := parser.GetNumbersFromLine(line)
			conversion := conversion{destinationRangeStart: numbers[0], sourceRangeStart: numbers[1], rangeValue: numbers[2]}
			currentMap = append(currentMap, conversion)
		} else if line == "" {
			maps = append(maps, currentMap)
			currentMap = []conversion{}
		}
	}

	if !reflect.DeepEqual(currentMap, []conversion{}) {
		maps = append(maps, currentMap)
	}

	return seeds, maps
}

func GetCorrespondingValue(inputValue int, conversions []conversion) int {
	outputValue := inputValue
	for _, conversion := range conversions {
		if (inputValue >= conversion.sourceRangeStart) && (inputValue < conversion.sourceRangeStart+conversion.rangeValue) {
			outputValue = conversion.destinationRangeStart + (inputValue - conversion.sourceRangeStart)
		}
	}

	return outputValue
}
