package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

type ball_count struct {
	colour string
	count  int
}

func main() {
	input, e := os.ReadFile(os.Args[1])
	if e != nil {
		panic(e)
	}

	inputValues := ParseInput(string(input))

	balls := make(map[string]int)
	balls["red"] = 12
	balls["green"] = 13
	balls["blue"] = 14

	var totalGameNumbers int = 0
	for gameNumber, game := range inputValues {
		if IsGamePossible(balls, game) {
			totalGameNumbers += gameNumber
		}
	}
	fmt.Printf("The total of the game IDs is: %d", totalGameNumbers)

	var totalPower = 0
	for _, game := range inputValues {
		totalPower += MinimumPowerOfGame(game)
	}
	fmt.Printf("The total power of the games is: %d", totalPower)
}

func ParseInput(input string) map[int][]ball_count {
	games := make(map[int][]ball_count)
	lines := strings.Split(input, "\n")
	for _, line := range lines {
		gameNumber, e := strconv.Atoi(strings.Split(line, ":")[0][5:])
		if e != nil {
			panic(e)
		}

		lineContent := strings.Split(line, ": ")[1]
		var gameValues []string
		gameValues = strings.FieldsFunc(lineContent, func(character rune) bool {
			if character == ',' || character == ';' {
				return true
			} else {
				return false
			}
		})

		var ball_counts []ball_count
		for _, gameValue := range gameValues {
			if gameValue[0] == ' ' {
				gameValue = gameValue[1:]
			}
			gameValueSplit := strings.Split(gameValue, " ")
			count, e := strconv.Atoi(gameValueSplit[0])
			if e != nil {
				panic(e)
			}
			ball_counts = append(ball_counts, ball_count{count: count, colour: gameValueSplit[1]})
		}
		games[gameNumber] = ball_counts
	}
	return games
}

func IsGamePossible(balls map[string]int, game []ball_count) bool {
	var possible bool = true
	for _, value_pair := range game {
		if value_pair.count > balls[value_pair.colour] {
			possible = false
		}
	}
	return possible
}

func MinimumPowerOfGame(game []ball_count) int {
	coloursMinimum := make(map[string]int)
	for _, value_pair := range game {
		if value_pair.count > coloursMinimum[value_pair.colour] {
			coloursMinimum[value_pair.colour] = value_pair.count
		}
	}
	var productOfColours int = -1
	for _, count := range coloursMinimum {
		if productOfColours == -1 {
			productOfColours = count
		} else {
			productOfColours = productOfColours * count
		}
	}
	return productOfColours
}
