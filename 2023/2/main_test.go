package main

import (
	"fmt"
	"reflect"
	"testing"
)

func TestParseInput(t *testing.T) {
	input := "Game 1: 1 red, 5 blue\nGame 2: 2 green; 1 blue"
	actualOutput := ParseInput(input)
	expectedOutput := make(map[int][]ball_count)
	expectedOutput[1] = []ball_count{{"red", 1}, {"blue", 5}}
	expectedOutput[2] = []ball_count{{"green", 2}, {"blue", 1}}

	if reflect.DeepEqual(actualOutput, expectedOutput) != true {
		t.Error("Did not equal the expected output")
	}
}

func TestIsGamePossible(t *testing.T) {
	balls := make(map[string]int)
	balls["red"] = 3
	balls["green"] = 4
	balls["blue"] = 5

	var tests = []struct {
		game     []ball_count
		possible bool
	}{
		{[]ball_count{
			{"red", 2},
			{"green", 3},
			{"blue", 4}}, true},
		{[]ball_count{
			{"red", 3},
			{"green", 4},
			{"blue", 5}}, true},
		{[]ball_count{
			{"red", 2},
			{"red", 3},
			{"red", 1}}, true},
		{[]ball_count{
			{"red", 2},
			{"red", 3},
			{"red", 4}}, false},
		{[]ball_count{
			{"red", 3},
			{"green", 4},
			{"blue", 6}}, false},
	}

	for _, test := range tests {
		testName := fmt.Sprint(test.game)
		t.Run(testName, func(t *testing.T) {
			gamePossible := IsGamePossible(balls, test.game)
			if gamePossible != test.possible {
				t.Errorf("Expected %t but got %t", test.possible, gamePossible)
			}
		})
	}

}

func TestMinimumPowerOfGame(t *testing.T) {
	var tests = []struct {
		game       []ball_count
		totalPower int
	}{
		{[]ball_count{
			{"red", 2},
			{"green", 3},
			{"blue", 4}},
			24},
		{[]ball_count{
			{"red", 3},
			{"green", 4},
			{"blue", 5}},
			60},
		{[]ball_count{
			{"red", 2},
			{"red", 3},
			{"red", 1}},
			3},
		{[]ball_count{
			{"red", 2},
			{"red", 3},
			{"red", 4}},
			4},
		{[]ball_count{
			{"red", 3},
			{"green", 4},
			{"blue", 6}},
			72},
	}

	for _, test := range tests {
		testName := fmt.Sprint(test.game)
		t.Run(testName, func(t *testing.T) {
			totalPower := MinimumPowerOfGame(test.game)
			if totalPower != test.totalPower {
				t.Errorf("Expected %d but got %d", test.totalPower, totalPower)
			}
		})
	}
}
