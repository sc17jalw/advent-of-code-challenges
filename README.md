# Advent of Code Challenges

This respository contains my solutions to the Advent of Code Challenges.

All credit to [Advent of Code](https://adventofcode.com/) for the great challenges.

Merge Requests with suggestions and improvements are welcome.

## Running Go programmes
1. Install [Golang](https://go.dev/doc/install)

2. Navigate to the directory of the module for a specific day.

3. Run `go test . -v` to run all unit tests with verbose output.

4. Run `go run . *args*` passing in arguments.

    There is normally one argument for each day with is the path to the input text file to read.