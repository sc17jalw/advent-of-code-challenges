package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	mostCalories, topThreeCalories := ParseInputAndGetMostCalories(os.Args[1])
	fmt.Printf("The largest total calorie count is %s\n", mostCalories)
	fmt.Printf("The top three total calorie counts are:\n%s\n", topThreeCalories)
}

func ParseInputAndGetMostCalories(filePath string) (string, string) {
	data, err := os.ReadFile(filePath)
	check(err)

	lines := strings.Split(string(data), "\n")
	for _, line := range lines {
		line = strings.ReplaceAll(line, "\r", "")
	}
	mostCalories := GetMostCalories(lines)
	multipleCalories1, multipleCalories2, multipleCalories3 := GetTopThreeCalories(lines)

	return fmt.Sprint(mostCalories), fmt.Sprintf("1st %d\n2nd %d\n3rd %d\nTotal: %d", multipleCalories1, multipleCalories2, multipleCalories3, (multipleCalories1 + multipleCalories2 + multipleCalories3))
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func GetMostCalories(lines []string) int {
	// Get an array of arrays for the values
	raindeersValues := GetRaindeerValues(lines)

	var total int = 0
	for _, raindeerValues := range raindeersValues {
		var raindeerTotal int = 0
		for _, value := range raindeerValues {
			raindeerTotal += value
		}

		if raindeerTotal > total {
			total = raindeerTotal
		}
	}

	return total
}

func GetTopThreeCalories(lines []string) (int, int, int) {
	// Get an array of arrays for the values
	raindeersValues := GetRaindeerValues(lines)

	var largestTotal1, largestTotal2, largestTotal3 int = 0, 0, 0

	for _, raindeerValues := range raindeersValues {
		var raindeerTotal int = 0
		for _, value := range raindeerValues {
			raindeerTotal += value
		}

		if (raindeerTotal > largestTotal1) && (raindeerTotal != largestTotal1) {
			largestTotal3 = largestTotal2
			largestTotal2 = largestTotal1
			largestTotal1 = raindeerTotal
		} else if (raindeerTotal > largestTotal2) && (raindeerTotal != largestTotal1) {
			largestTotal3 = largestTotal2
			largestTotal2 = raindeerTotal
		} else if (raindeerTotal > largestTotal3) && (raindeerTotal != largestTotal1) && (raindeerTotal != largestTotal2) {
			largestTotal3 = raindeerTotal
		}
	}

	return largestTotal1, largestTotal2, largestTotal3
}

func GetRaindeerValues(lines []string) [][]int {
	var raindeers_totals [][]int
	var current_raindeer []int
	var current_raindeer_number int = 0
	for _, line := range lines {
		if number, error := strconv.Atoi(line); error == nil {
			current_raindeer = append(current_raindeer, number)
		} else {
			raindeers_totals = append(raindeers_totals, current_raindeer)
			current_raindeer_number++
			current_raindeer = []int{}
		}
	}
	if len(current_raindeer) != 0 {
		raindeers_totals = append(raindeers_totals, current_raindeer)
	}
	return raindeers_totals
}
