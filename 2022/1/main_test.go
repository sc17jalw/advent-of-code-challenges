package main

import (
	"reflect"
	"testing"
)

func TestGetTopThreeCalories(t *testing.T) {
	var lines []string
	lines = append(lines, "1000")
	lines = append(lines, "2000")
	lines = append(lines, "3000")
	lines = append(lines, "")
	lines = append(lines, "4000")
	lines = append(lines, "5000")
	lines = append(lines, "")
	lines = append(lines, "6000")
	lines = append(lines, "")
	lines = append(lines, "7000")
	lines = append(lines, "8000")
	lines = append(lines, "9000")

	actual1, actual2, actual3 := GetTopThreeCalories(lines)
	if actual1 != 24000 {
		t.Errorf("Expected 1st largest calorie total to equal 24000, but was: %d", actual1)
	}

	if actual2 != 9000 {
		t.Errorf("Expected 2nd largest calorie total to equal 9000, but was: %d", actual2)
	}

	if actual3 != 6000 {
		t.Errorf("Expected 3rd largest calorie total to equal 6000, but was: %d", actual3)
	}
}

func TestMostCalories(t *testing.T) {
	var lines []string
	lines = append(lines, "1000")
	lines = append(lines, "2000")
	lines = append(lines, "")
	lines = append(lines, "3000")
	lines = append(lines, "4000")

	actual := GetMostCalories(lines)
	if actual != 7000 {
		t.Errorf("Expected 7000, but was: %d", actual)
	}
}

func TestGetRaindeerValues(t *testing.T) {
	var lines []string
	lines = append(lines, "1000")
	lines = append(lines, "2000")
	lines = append(lines, "")
	lines = append(lines, "3000")
	lines = append(lines, "4000")
	actual := GetRaindeerValues(lines)

	var expected [][]int
	expected = append(expected, []int{1000, 2000})
	expected = append(expected, []int{3000, 4000})

	if !reflect.DeepEqual(expected, actual) {
		t.Error("Expected value did not equal the actual value")
	}
}

func TestParseInput(t *testing.T) {
	var lines []string
	lines = append(lines, "1000")
	lines = append(lines, "2000")
	lines = append(lines, "3000")
	lines = append(lines, "")
	lines = append(lines, "4000")
	lines = append(lines, "5000")
	lines = append(lines, "")
	lines = append(lines, "6000")
	lines = append(lines, "")
	lines = append(lines, "7000")
	lines = append(lines, "8000")
	lines = append(lines, "9000")

	actual := GetMostCalories(lines)
	if actual != 24000 {
		t.Errorf("Output did not match expected")
	}
}

func TestGetAnswer(t *testing.T) {
	actualOutput, _ := ParseInputAndGetMostCalories("./input.txt")
	if "74394" != actualOutput {
		t.Error("Test failed")
	}
}
